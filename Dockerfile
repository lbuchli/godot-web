FROM emscripten/emsdk:3.1.53 as builder

ARG GODOT_VERSION=bd70b8e1f643dbf7252be9bc367e0de0f82d722d # https://github.com/godotengine/godot/pull/85939
ARG USER=builder

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
  build-essential \
  scons \
  pkg-config \
  libx11-dev \
  libxcursor-dev \
  libxinerama-dev \
  libgl1-mesa-dev \
  libglu-dev \
  libasound2-dev \
  libpulse-dev \
  libudev-dev \
  libxi-dev \
  libxrandr-dev \
 && rm -rf /var/lib/apt/lists/*

RUN useradd -m ${USER}

USER ${USER}
ENV USER=${USER}

RUN git clone https://github.com/godotengine/godot.git ~/build-godot \
 && cd ~/build-godot \
 && git reset --hard ${GODOT_VERSION}

WORKDIR /home/${USER}/build-godot
RUN scons platform=web tools=no target=template_release threads=no \
 && mv bin/godot.web.template_release.wasm32.nothreads.zip bin/web_nothreads_release.zip
RUN scons platform=linuxbsd target=editor

FROM ubuntu:22.04

COPY --from=builder /home/*/build-godot/bin/godot.linuxbsd.editor.x86_64 /usr/bin/godot
COPY --from=builder /home/*/build-godot/bin/web_nothreads_release.zip /root/.local/share/godot/export_templates/4.3.dev/
